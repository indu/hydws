FROM python:3.10-slim as builder

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update \
    && apt-get install -y --no-install-recommends build-essential curl libpq-dev git\
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && apt-get clean


COPY requirements.txt .
RUN pip wheel --no-cache-dir --no-deps --wheel-dir /app/wheels -r requirements.txt



FROM python:3.10-slim as app

LABEL maintainer="Nicolas Schmid <nicolas.schmid@sed.ethz.ch>"

WORKDIR /hydws

RUN apt-get update \
    && apt-get install -y --no-install-recommends build-essential curl libpq-dev git\
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && apt-get clean \
    && useradd --create-home python \
    && chown python:python -R /hydws

USER python

ENV PYTHONUNBUFFERED="true" \
    PYTHONDONTWRITEBYTECODE="true" \
    PYTHONPATH="." \
    PATH="${PATH}:/home/python/.local/bin" \
    USER="python"

COPY --from=builder --chown=python:python /app/wheels /wheels
COPY --from=builder --chown=python:python /app/requirements.txt .

RUN pip install --no-cache --user /wheels/*

COPY --chown=python:python . .

EXPOSE 8000

# CMD ["gunicorn", "-k", "uvicorn.workers.UvicornWorker", "-c", "python:config.gunicorn", "hydws.main:app"]
CMD ["uvicorn", "hydws.main:app", "--port", "8000", "--workers", "1", "--timeout-keep-alive", "300", "--host", "0.0.0.0"]
